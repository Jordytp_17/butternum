/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemasEcuaciones;

import Calculos_Datos.RJacobi;
import java.awt.Font;
import java.awt.Point;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import org.netbeans.lib.awtextra.AbsoluteConstraints;

/**
 *
 * @author nesto
 */
public class Jacobi extends javax.swing.JDialog {

    /**
     * Creates new form Jacobi
     */
    DefaultTableModel modelo;
    private JTable jTable1 = new javax.swing.JTable();
    JScrollPane jScrollPane1 = new JScrollPane();
    public String sistema;
    private ArrayList<JLabel> listaLabel = new ArrayList<JLabel>();
    public int filas;
    public ArrayList<Float> matrizReal = new ArrayList<Float>();
    public ArrayList<Float> iniciales;
    public double error;

    public Jacobi(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea4 = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextArea5 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setText("Solucion por el metodo de Jacobi");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 25, -1, -1));

        jLabel2.setText("Sistema");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(55, 67, -1, -1));

        jLabel4.setText("Variable Despejadas");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, -1, -1));

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setName("jLabel3"); // NOI18N
        jScrollPane2.setViewportView(jTextArea1);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 150, 70));

        jTextArea2.setColumns(20);
        jTextArea2.setRows(5);
        jTextArea2.setName("jLabel5"); // NOI18N
        jScrollPane3.setViewportView(jTextArea2);

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 190, 150, 60));

        jLabel3.setText("Verificacion de que la matriz sea diagonalmente dominante");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 80, -1, -1));

        jTextArea3.setColumns(20);
        jTextArea3.setRows(5);
        jScrollPane4.setViewportView(jTextArea3);

        jPanel1.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 110, 320, -1));

        jLabel5.setText("La matriz es diagonalmente dominante");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 210, -1, -1));

        jTextArea4.setColumns(20);
        jTextArea4.setRows(5);
        jScrollPane5.setViewportView(jTextArea4);

        jPanel1.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 90, 150, -1));

        jLabel6.setText("Soluciones");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 60, -1, -1));

        jLabel7.setText("Comprobaciones");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 190, -1, -1));

        jTextArea5.setColumns(20);
        jTextArea5.setRows(5);
        jScrollPane6.setViewportView(jTextArea5);

        jPanel1.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 210, 160, -1));

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        //setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jTextArea1.setText(sistema);
        int psX = 250, psY = 100;
        String validacion = "";
        double resto = 0;
        for (int i = 0; i < filas; i++) {
            for (int y = 0; y < filas; y++) {
                resto = resto + Math.abs(matrizReal.get(i * (filas + 1) + y));
            }
            resto = resto - Math.abs(matrizReal.get((filas + 1) * i + i));
            validacion = validacion + "Valor absoluto del elemento x" + subIndices(i + 1) + " en la fila " + (i + 1) + " =       "
                    + String.valueOf(Math.abs(matrizReal.get((filas + 1) * i + i))) + " > " + resto + "       = " + "Suma del valor absoluto del resto de los elementos de la fila " + (i + 1) + "\n";
                    resto=0;
        }
        jTextArea3.setText(validacion);

        jLabel4.setLocation(new Point(50, 170));
        jTextArea2.setLocation(new Point(jLabel4.getLocation().x, jLabel4.getLocation().y + jLabel4.getHeight() + 20));
        String ecuaciones = "";
        String sustituciones = "";
        for (int i = 0; i < filas; i++) {
            ecuaciones = ecuaciones + "x" + subIndices(i + 1) + " = " + "b" + subIndices(i + 1) + "/a" + subIndices(i + 1) + "\u00b8" + subIndices(i + 1);
            for (int y = 0; y < filas; y++) {
                if (y != i) {
                    ecuaciones = ecuaciones + " -" + "(a" + subIndices(i + 1) + "\u00b8" + subIndices(y + 1) + "/a" + subIndices(i + 1) + "\u00b8" + subIndices(i + 1) + ")*x" + subIndices(y + 1);
                }
            }
            sustituciones = " = " + String.valueOf(matrizReal.get((i + 1) * (filas + 1) - 1)) + "/" + String.valueOf(matrizReal.get(i * (filas + 1) + i));
            for (int y = 0; y < filas; y++) {
                if (y != i) {
                    sustituciones = sustituciones + " - (" + String.valueOf(matrizReal.get(i * (filas + 1) + y)) + "/" + String.valueOf(matrizReal.get(i * (filas + 1) + i)) + ")*x" + subIndices(y + 1);
                }
            }
            ecuaciones = ecuaciones + sustituciones + "\n";
            sustituciones = "";
        }
        jTextArea2.setText(ecuaciones);

        String[] columnas = new String[2 + 2 * filas];
        columnas[0] = "IT";
        for (int i = 0; i < filas; i++) {
            columnas[i + 1] = "X" + subIndices(i + 1);
        }
        for (int i = 0; i < filas; i++) {
            columnas[i + 1 + filas] = "ErrorX" + subIndices(i + 1);
        }
        columnas[1 + 2 * filas] = "C.Parada";

        jTable1.setModel(new javax.swing.table.DefaultTableModel(columnas, 1));
        jScrollPane1.setViewportView(jTable1);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 338, 625, 170));
        jTable1.setLocation(60, 300);
        RJacobi solucion = new RJacobi();
        ArrayList<Float> datos = solucion.MetodoDeJacobi(matrizReal, filas, error, iniciales);

        modelo = (DefaultTableModel) jTable1.getModel();
        
        String [] row = new String[2+filas*2];
        for (int i = 0; i < datos.size(); i = i + (2 * filas + 2)) {
            
            for (int y = 0; y < 2 * filas + 1; y++) {
                row[y] = String.valueOf((datos.get(i + y)));
            }
            if (datos.get(i + 2 * filas + 1) == 0) {
                row[2 * filas + 1] = "Falso";
            } else {
                row[2 * filas + 1] = "Verdadero";
            }
            modelo.addRow(row);
        }
        String soluciones="";
        for(int i=0;i<filas;i++){
            soluciones=soluciones+"X"+subIndices(i+1)+" = "+datos.get(datos.size()-(2+2*filas)+i+1)+"\n";            
        }        
        jTextArea4.setText(soluciones);
        String comprobacion="";
        String filaDatos="";
        String filaValores="";
        float resultado=0;
        for(int i=0;i<filas;i++){

            for (int y = 0; y < filas; y++) {
                filaDatos = filaDatos + matrizReal.get(y + i * (filas + 1)) + " X" + subIndices(y + 1) + " + ";
            }
            comprobacion = comprobacion + filaDatos + " = ";
            for (int y = 0; y < filas; y++) {
                filaValores = filaValores + matrizReal.get(y + i * (filas + 1)) + "(" + datos.get(datos.size() - (2 + 2 * filas) + y + 1) + ") + ";
            }
            comprobacion=comprobacion+filaValores+" = ";
            for (int y = 0; y < filas; y++) {
                resultado = resultado + matrizReal.get(y + i * (filas + 1)) *datos.get(datos.size() - (2 + 2 * filas) + y + 1);
            }
            comprobacion=comprobacion+resultado+"\n"; 
            resultado=0;
            filaValores="";
            filaDatos="";
        }        
        jTextArea5.setText(comprobacion);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_formWindowOpened

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Jacobi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Jacobi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Jacobi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Jacobi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Jacobi dialog = new Jacobi(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    public String subIndices(int caso) {
        String valor = "";
        switch (caso) {
            case 1:
                valor = "\u2081";
                break;
            case 2:
                valor = "\u2082";
                break;
            case 3:
                valor = "\u2083";
                break;
            case 4:
                valor = "\u2084";
                break;
            case 5:
                valor = "\u2085";
                break;
            case 6:
                valor = "\u2086";
                break;
            case 7:
                valor = "\u2087";
                break;
            case 8:
                valor = "\u2088";
                break;
            case 9:
                valor = "\u2089";
                break;
            case 0:
                valor = "\u2080";
                break;
            default:
                JOptionPane.showMessageDialog(null, "No fuerce al programa, ingrese una matriz de menos de 10 filas", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
                valor = "n";
                break;
        }
        return valor;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JTextArea jTextArea4;
    private javax.swing.JTextArea jTextArea5;
    // End of variables declaration//GEN-END:variables
}
