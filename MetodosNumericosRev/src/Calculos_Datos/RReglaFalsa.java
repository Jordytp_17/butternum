/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculos_Datos;

import java.util.ArrayList;
import Calculos_Datos.MathFuntion;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nesto
 */
public class RReglaFalsa {
    
        MathFuntion formula;
        double xi, xs;
        public ArrayList<Float> MetodoReglaFalsa(String Ecaucion1, float xi, float xs,float errort)
        {
            String Ecaucion="0*0+"+Ecaucion1;
            try {
                formula= new MathFuntion(Ecaucion);
                ArrayList<Float> table = new ArrayList<Float>();
                this.xi = xi;
                this.xs = xs;
                
                float error = 1;
                float xr = 0;
                int it = 0;
                table.add(Float.valueOf(++it));
                table.add(xi);
                table.add(xs);
                xr = xs - ((formula.eval(((xs)))) *
                        (xi - xs)) / ((formula.eval(((xi))))
                        - (formula.eval(((xs)))));
                table.add(xr);
                table.add((formula.eval(((xi)))));
                table.add((formula.eval(((xs)))));
                table.add((formula.eval(((xr)))));
                table.add((formula.eval(((xi))))
                        * (formula.eval(((xr)))));
                table.add(xr);
                table.add(0F);
                while (error > errort)
                {
                    table.add(Float.valueOf(++it));
                    
                    if (Double.valueOf(formula.eval(((xr)))) == 0)
                    {
                        error = 0;
                    }
                    else if (table.get(table.size() - 4) < 0)
                    {
                        xs = xr;
                    }
                    else
                    {
                        xi = xr;
                    }
                    table.add(xi);
                    table.add(xs);
                    xr = xs - ((formula.eval(((xs)))) * (xi - xs)) / ((formula.eval(((xi)))) - (formula.eval(((xs)))));
                    table.add(xr);
                    table.add((formula.eval(((xi)))));
                    table.add((formula.eval(((xs)))));
                    table.add((formula.eval(((xr)))));
                    table.add((formula.eval(((xi)))) * (formula.eval(((xr)))));
                    error = Math.abs(table.get(table.size() - 5) - table.get(table.size() - 15));
                    table.add(error);
                    if (table.get(table.size() - 1) > errort)
                    {
                        table.add(0F);
                    }
                    else
                    {
                        table.add(1F);
                    }
                }
                return table;
            } catch (Exception ex) {
                Logger.getLogger(RReglaFalsa.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
}
