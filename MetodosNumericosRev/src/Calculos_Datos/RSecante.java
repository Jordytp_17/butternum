/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculos_Datos;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nesto
 */
public class RSecante {
    
        MathFuntion formula; 
        double x0, x1;
        public ArrayList<Float> MetodoSecante(String Ecaucion, float x0, float x1,float tolerancia)
        {
            try {
                formula= new MathFuntion(Ecaucion);
                ArrayList<Float> table = new ArrayList<Float>();
                this.x0 = x0;
                this.x1 = x1;
                
                float error = 1;
                float xi = 0;
                int it = 0;
                table.add(Float.valueOf(++it));
                table.add(x0);
                table.add((formula.eval(((x0)))));
                table.add(x0);
                table.add(0F);
                table.add(Float.valueOf(++it));
                table.add(x1);
                table.add((formula.eval(((x1)))));
                table.add(Math.abs(x0-x1));
                table.add(0F);
                while (error>tolerancia)
                {
                    table.add(Float.valueOf(++it));
                    table.add(table.get(table.size()-5)-((table.get(table.size() - 5)- table.get(table.size() - 10))* table.get(table.size() - 4))
                            /(table.get(table.size() - 4)- table.get(table.size() - 9)));
                    table.add(((formula.eval(((table.get(table.size()-1)))))));
                    error = Math.abs(table.get(table.size()-2)-table.get(table.size()-7));
                    table.add(error);
                    if (error>tolerancia)
                    {
                        table.add(0F);
                    }
                    else
                    {
                        table.add(1F);
                    }
                }
                return table;
            } catch (Exception ex) {
                Logger.getLogger(RSecante.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
}
