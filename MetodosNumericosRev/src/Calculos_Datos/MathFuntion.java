/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculos_Datos;

import com.bestcode.mathparser.IMathParser;
import com.bestcode.mathparser.MathParserFactory;

/**
 *
 * @author nesto
 */
public class MathFuntion {
    String definicion;
    IMathParser parser= MathParserFactory.create();
    public MathFuntion(String def){
        definicion=def;
        parser.setExpression(def);
    }
    public MathFuntion(){
    }
    public float eval(float x) throws Exception{
        float r=Float.NaN;
        parser.setX(x);
        r=(float)parser.getValue();
        return r;
    }
    public float[] eval(float[] x) throws Exception{
        int n=x.length;
        float[] r= new float[n];
        for(int i=0;i<n;i++){
         parser.setX(x[i]);
        r[i]=(float)parser.getValue();           
        }       
        return r;
    }
    public float[] rango(float x0,float xn,float d){
        int n=(int)(Math.abs(xn-x0)/d);
        float [] r= new float[n];
        for(int i=0;i<n;i++){
            r[i]=x0+i*d;
        }
        return r;
    }
    
}
