/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculos_Datos;

import java.util.ArrayList;

/**
 *
 * @author nesto
 */
public class RNewtonRhapson {
    
        MathFuntion formula; 
        double x0;
        public ArrayList<Float> MetodoNewtonRapson(String Ecaucion, float x0,float tolerancia) throws Exception
        {
            formula= new MathFuntion(Ecaucion);
            ArrayList<Float> table = new ArrayList<Float>();
            this.x0 = x0;            
            
            float error = 1;
            
            int it = 0;
            table.add(Float.valueOf(++it));
            table.add(x0);
            table.add((formula.eval(((x0)))));
            table.add(((formula.eval(((x0-0.001F))))- (formula.eval(((x0+0.002F))))) /(-0.02F));
            table.add(x0);
            table.add(0F);
            while (error > tolerancia)
            {
                table.add(Float.valueOf(++it));
                table.add(table.get(table.size()-6)-((table.get(table.size()-5))/(table.get(table.size() - 4))));                
                table.add((formula.eval(((table.get(table.size()-1))))));
                table.add(((formula.eval((((table.get(table.size()-2))-1))))- 
                    (formula.eval((((table.get(table.size() - 2)) +1))))) /(-2));
                error = Math.abs(table.get(table.size() - 3) - table.get(table.size() - 9));
                table.add(error);
                if (error>tolerancia)
                {
                    table.add(0F);
                }
                else
                {
                    table.add(1F);
                }
            }
            
            return table;
        }
}
