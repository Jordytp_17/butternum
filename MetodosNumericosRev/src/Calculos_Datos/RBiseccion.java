/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculos_Datos;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import Calculos_Datos.MathFuntion;


/**
 *
 * @author nesto
 */
public class RBiseccion {
     private MathFuntion formula;
     private MathFuntion formula2;

    
 public ArrayList<Float> MetodoDeBiseccion(String Ecaucion,float a,float b,float error) throws Exception{
     
     formula = new MathFuntion(Ecaucion);
     formula2 = new MathFuntion("0*0");
            ArrayList<Float> table = new ArrayList<Float>();
            float intervaloI = a, intervaloD = b;
            float error2 = 1F;
            float xi = 0F;
            int it = 0;
            while (error2 > error)
            {
                it++;
                table.add((float)(it));
                table.add(intervaloI);
                table.add((intervaloD));
                xi = (intervaloI + intervaloD) / 2;
             
                if ((formula.eval(xi)) == 0)
                {
                    error2 = 0;
                }
                else if ((formula.eval(((xi+0F))) * formula.eval(((intervaloI+0F)))) < 0)
                {
                    intervaloD = xi;
                }
                else
                {
                    intervaloI = xi;
                }

                table.add(xi);
                table.add((formula.eval(((table.get(table.size()-3)+0F))))-formula2.eval(((table.get(table.size()-3)+0F))));
                table.add((formula.eval(((xi+0F))))-formula2.eval((xi+0F)));
                table.add(table.get(table.size()-1)*table.get(table.size()-2));
                if (table.size()-13>0) 
                {
                    table.add(Math.abs(table.get(table.size() - 13) - xi));
                }
                else
                {
                    table.add(xi);
                }
                if (table.get(table.size()-1)> error)
                {
                    table.add(0F);
                }
                else
                {
                    table.add(1F);
                }             
                
                if (table.size() > 8)
                {
                    error2 = table.get(table.size()-2);
                }                
            }                      
            return table;     
    }   

}
