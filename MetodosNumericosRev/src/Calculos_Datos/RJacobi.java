/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculos_Datos;

import java.util.ArrayList;

/**
 *
 * @author nesto
 */
public class RJacobi {
         public ArrayList<Float> MetodoDeJacobi(ArrayList<Float> elementos, int filas,double error,ArrayList<Float> iniciales)
        {
            ArrayList<Float> lista = new ArrayList<Float>();
            int contador=0;
            lista.add((float)++contador);
            for (int i=0;i<filas;i++)
            {
                lista.add(iniciales.get(i));
            }
            for (int i = 0; i < filas; i++)
            {
                lista.add(iniciales.get(i));
            }
            lista.add(0F);
            double errorr = 1;
            while (errorr > error)
            {
                lista.add((float)++contador);
                float valorEsperado = 0;
                for (int y = 0; y < filas; y++)
                {
                    valorEsperado = elementos.get((y + 1) * (filas + 1) - 1)/elementos.get(y*(filas+1)+y);
                    for (int i = 0; i < filas; i++)
                    {
                        if (i != y)
                        {
                            valorEsperado = valorEsperado - elementos.get((y) * (filas + 1) + i) * lista.get((contador-2)*(2*filas+2)+i+1)/elementos.get(y*(filas+1)+y);
                        }
                    }
                    lista.add(valorEsperado);
                }
                float errorr2 = 0,errorr3=0;
                boolean valorVerdad = false;
                for (int y = 0; y < filas; y++)
                {
                    errorr2 = (Math.abs(lista.get((contador-2)*(2*filas+2)+(y+1)) - lista.get((contador - 1) * (2 * filas + 2)+ (y + 1))));
                    lista.add(errorr2);
                    if (errorr2<error)
                    {
                        valorVerdad = true;
                    }
                    else
                    {
                        valorVerdad = false;
                    }
                }
                
                if (valorVerdad==false)
                {
                    lista.add(0F);
                    errorr = 1;
                }
                else
                {
                    lista.add(1F);
                    errorr = error - 1;
                }
            }
            return lista;   
            }
}