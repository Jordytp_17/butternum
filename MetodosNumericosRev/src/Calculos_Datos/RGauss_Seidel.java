/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculos_Datos;

import java.util.ArrayList;

/**
 *
 * @author nesto
 */
public class RGauss_Seidel {
    
        public ArrayList<Float> MetodoDeSeidel(ArrayList<Float> elementos, int filas, float error, ArrayList<Float> iniciales)
        {
            ArrayList<Float> lista = new ArrayList<Float>();
            int contador = 0;
            lista.add((float)++contador);
            for (int i = 0; i < filas; i++)
            {
                lista.add(iniciales.get(i));
            }
            for (int i = 0; i < filas; i++)
            {
                lista.add(iniciales.get(i));
            }
            for (int i = 0; i < filas; i++)
            {
                lista.add(0F);
            }
            double errorr = 1;
            while (errorr >error)
            {
                lista.add((float)++contador);
                float valorEsperado = 0;
                float errorr2 = 0, errorr3 = 0;
                for (int y = 0; y < filas; y++)
                {
                    valorEsperado = elementos.get((y + 1) * (filas + 1) - 1)/ elementos.get(y * (filas + 1) + y);
                    for (int i = 0; i < y; i++)
                    {
                            valorEsperado = valorEsperado - elementos.get((y) * (filas + 1) + i)* lista.get((contador - 1) * (3 * filas + 1) + 2 * i+1) / elementos.get(y * (filas + 1) + y);                        
                    }
                    for (int i = y+1; i < filas; i++)
                    {                        
                            valorEsperado = valorEsperado - elementos.get((y)* (filas + 1) + i) * lista.get((contador - 2) * (3 * filas + 1) + 2*i + 1) / elementos.get(y * (filas + 1) + y);                        
                    }
                    lista.add(valorEsperado);
                    errorr2 = (Math.abs(valorEsperado - lista.get(lista.size()-3*filas-2)));
                    lista.add(errorr2);
                    if (errorr2 > errorr3)
                    {
                        errorr3 = errorr2;
                    }
                }

                errorr=errorr3;
                for (int i=0;i<filas;i++)
                {
                    if (lista.get((contador-1)*(3*filas+1)+2*(i+1))>error)
                    {
                        lista.add(0F);
                    }
                    else
                    {
                        lista.add(1F);
                    }
                }

            }
            return lista;
        }
  
}
