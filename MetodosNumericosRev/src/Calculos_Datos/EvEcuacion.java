/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculos_Datos;

import java.util.ArrayList;

/**
 *
 * @author nesto
 */
public class EvEcuacion {

    public ArrayList<String> getTokens(String expression) {
        String operators = "()^*/-+lcrastvdy";
        ArrayList<String> tokens = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        char[] funcionChar = expression.replace(" ", "").toCharArray();

        for (int i = 0; i < funcionChar.length; i++) {
            if (operators.indexOf(funcionChar[i]) >= 0) {
                if ((i - 1) >= 0) {
                    if (operators.indexOf(funcionChar[i]) == 5 && Character.isDigit(funcionChar[i + 1]) && Character.isDigit
                        (funcionChar[i - 1]) == false && funcionChar[i - 1] != ')')
		{
							
			sb.append(funcionChar[i]);
                }else if (operators.indexOf(funcionChar[i]) == 5 && Character.isDigit(funcionChar[i + 1]) && Character.isDigit
                                (funcionChar[i - 1]) == false && funcionChar[i - 1] == ')')
                        {                            
							tokens.add(String.valueOf(funcionChar[i]));
                    }
                    else if (operators.indexOf(funcionChar[i]) == 6 && Character.isDigit(funcionChar[i + 1]) && Character.isDigit
                                (funcionChar[i - 1]) == false && funcionChar[i - 1] != ')'
                    
                        )
						{
							String funcion = "";
                        for (int y = 0; y < funcionChar.length; y++) {
                            funcion = funcion + String.valueOf(funcionChar[y]);
                        }
                        
                        tokens.add(String.valueOf(funcionChar[i]));
                    }else if (operators.indexOf(funcionChar[i]) == 6 && Character.isDigit(funcionChar[i + 1]) && Character.isDigit
                                (funcionChar[i - 1]) == false && funcionChar[i - 1] == ')'
                    
                        )
                        {
							sb.append(funcionChar[i]);
                    }
                    else
						{
							if ((sb.length() > 0))
							{
								tokens.add(String.valueOf(sb));
								sb.setLength(0);
							}
							tokens.add(String.valueOf(funcionChar[i]));
						}
                } else {
                    if (operators.indexOf(funcionChar[i]) == 5 && Character.isDigit(funcionChar[i + 1])) {
                        sb.append(funcionChar[i]);
                    } else if (operators.indexOf(funcionChar[i]) == 6 && Character.isDigit(funcionChar[i + 1])) {
                        sb.append(funcionChar[i]);
                    } else {
                        if ((sb.length() > 0)) {
                            tokens.add(String.valueOf(sb));
                            sb.setLength(0);
                        }
                        tokens.add(String.valueOf(funcionChar[i]));
                    }
                }
            } else if (funcionChar[i] == 'x') {
                tokens.add(String.valueOf(funcionChar[i]));
            } else {
                sb.append(funcionChar[i]);
            }
        }

        if ((sb.length() > 0)) {
            tokens.add(String.valueOf(sb));
        }
        return tokens;
    }

    public String ValorEsperado(String expression) {
        String funcion = expression;
        int contParentesis = 0;
        ArrayList<String> stokeFuncion = new ArrayList<String>();
        ArrayList<String> stokenFuncionArreglos = new ArrayList<String>();
        for(int c=0;c<expression.toCharArray().length;c++)
        {
	if (expression.toCharArray()[c] == '(') 
        {
            contParentesis++;
        }
        }

        do {
            stokeFuncion = EvaluarSegunLista(funcion);
            for (int i = 0; i < stokeFuncion.size(); i++) {
                if (stokeFuncion.get(i).toCharArray()[0] == '(') {
                    if (Character.isDigit
                        (stokeFuncion.get(i + 1).toCharArray()[stokeFuncion.get(i + 1).toCharArray().length - 1]) && stokeFuncion.get(i + 2).toCharArray()[stokeFuncion.get(i + 2).toCharArray().length - 1] == ')'
                    
                        )
						{
							stokeFuncion.remove(i + 2);
                        stokeFuncion.remove(i);
                        contParentesis = contParentesis - 1;
                    }
                }
            }
            funcion = "";
            for (int i = 0; i < stokeFuncion.size(); i++) {
                funcion = funcion + stokeFuncion.get(i);
            }
        } while (contParentesis > 0);
        			
        funcion = "";
        for (int i = 0; i < stokeFuncion.size(); i++) {
            funcion = funcion + stokeFuncion.get(i);
        }
        stokeFuncion = EvaluarSegunLista(funcion);

        funcion = "";
        for (int i = 0; i < stokeFuncion.size(); i++) {
            funcion = funcion + stokeFuncion.get(i);
        }
        stokeFuncion = EvaluarSegunLista(funcion);
        funcion = "";
        for (int i = 0; i < stokeFuncion.size(); i++) {
            funcion = funcion + stokeFuncion.get(i);
        }

        return funcion;
    }

    public ArrayList<String> EvaluarSegunLista(String expression) {
        ArrayList<String> partesFuncion = getTokens(expression);
        ArrayList<String> partesFuncionMove = new ArrayList<String>();
        int multContador = 0, divContador = 0, sumContador = 0, restContador = 0, logContador = 0, cosContador = 0, powContador = 0, sqrtContador = 0, absContador = 0, senoContador = 0, tanContador = 0, arcCosContador = 0, arcSenContador = 0, arcTanContador = 0;
        String caracter = "";
        for (int k = 0; k < partesFuncion.size(); k++) {
            caracter = partesFuncion.get(k);
            if (caracter.equals("*")) {
                multContador++;
            } else if (caracter.equals("/")) {
                divContador++;
            } else if (caracter.equals("+")) {
                sumContador++;
            } else if (caracter.equals("-")) {
                restContador++;
            } else if (caracter.equals("l")) {
                logContador++;
            } else if (caracter.equals("c")) {
                cosContador++;
            } else if (caracter.equals("^")) {
                powContador++;
            } else if (caracter.equals("r")) {
                sqrtContador++;
            } else if (caracter.equals("a")) {
                absContador++;
            } else if (caracter.equals("s")) {
                senoContador++;
            } else if (caracter.equals("t")) {
                tanContador++;
            } else if (caracter.equals("v")) {
                arcCosContador++;
            } else if (caracter.equals("d")) {
                arcSenContador++;
            } else if (caracter.equals("y")) {
                arcTanContador++;
            }
        }

        String Numfuntion = "";
        for (int j = 0; j < partesFuncion.size(); j++) {
            Numfuntion = Numfuntion + "|" + partesFuncion.get(j);
        }

        while (multContador > 0) {
            int p = partesFuncion.size();
            for (int i = 0; i < partesFuncion.size(); i++) {
                if (partesFuncion.size() > i) {

                    if (partesFuncion.get(i).toCharArray()[partesFuncion.get(i).toCharArray().length - 1] == '*') {

                        if (Character.isDigit
                            (partesFuncion.get(i - 1).toCharArray()[(partesFuncion.get(i - 1).toCharArray().length - 1)])
                                    && (Character.isDigit(partesFuncion.get(i + 1).toCharArray()[(partesFuncion.get(i + 1).toCharArray().length - 1)])) &&
								((partesFuncion.get(i + 1).toCharArray()[0]
                        ) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0])) &&
								((partesFuncion.get(i - 1).toCharArray()[0]
                        ) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
								|| partesFuncion.get(i + 1).toCharArray()[0] == '-' || partesFuncion.get(i + 1).toCharArray()[0] == '+'
                        
                            )
							{
								partesFuncionMove.remove(partesFuncionMove.size() - 1);
                            partesFuncionMove.add(String.valueOf(Double.valueOf
                            (partesFuncion.get(i - 1)) * Double.valueOf
                            (partesFuncion.get(i + 1)
                            ))
                            );
								if ((i + 2) < partesFuncion.size()) {
                                for (int a = i + 2; a < partesFuncion.size(); a++) {
                                    partesFuncionMove.add(partesFuncion.get(a));
                                }
                                partesFuncion.clear();
                                for (int b = 0; b < partesFuncionMove.size(); b++) {
                                    partesFuncion.add(partesFuncionMove.get(b));
                                }
                                partesFuncionMove.clear();
                            } else {
                                partesFuncion.clear();
                                for (int b = 0; b < partesFuncionMove.size(); b++) {
                                    partesFuncion.add(partesFuncionMove.get(b));
                                }
                                partesFuncionMove.clear();
                            }
                            break;
                        }
                        else
							{
								partesFuncionMove.add(partesFuncion.get(i));
							}

                    } else {
                        partesFuncionMove.add(partesFuncion.get(i));
                    }
                    String funtion = "";
                    for (int j = 0; j < partesFuncion.size(); j++) {
                        funtion = funtion + partesFuncion.get(j);
                    }
                }
            }
            multContador--;
        }
        partesFuncionMove.clear();
        while (divContador > 0) {
            int p = partesFuncion.size();
            for (int i = 0; i < partesFuncion.size(); i++) {
                if (partesFuncion.get(i).toCharArray()[partesFuncion.get(i).toCharArray().length - 1] == '/') {
                    if (Character.isDigit
                        (partesFuncion.get(i - 1).toCharArray()[(partesFuncion.get(i - 1).toCharArray().length - 1)]) && Character.isDigit(partesFuncion.get(i + 1).toCharArray()[(partesFuncion.get(i + 1).toCharArray().length - 1)])
							&& ((partesFuncion.get(i + 1).toCharArray()[0]
                    ) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0])) && ((partesFuncion.get(i - 1).toCharArray()[0]
                    ) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0])
                    
                        ))
						{
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.add(String.valueOf(Double.valueOf
                        (partesFuncion.get(i - 1)) / Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();

                        }
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            divContador--;
        }
        partesFuncionMove.clear();
       
			while (sumContador > 0)
			{
				for (int i = 0; i < partesFuncion.size(); i++)
				{
					if ((i <= 1 || i >= partesFuncion.size() - 2) && (partesFuncion.get(i).toCharArray()[partesFuncion.get(i).length() - 1] == '+'))
					{
						if ((i == 1 && partesFuncion.size() - 1 <= 2) && Character.isDigit(partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
							&& Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0])))
						{
							//Aqui solo es cuando hay tres elemntos y esta en el final e inicio al mismo tiempo
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
							partesFuncionMove.add(String.valueOf(Double.valueOf(partesFuncion.get(i - 1)) + Double.valueOf(partesFuncion.get(i + 1))));
							if ((i + 2) < partesFuncion.size())
							{
								for (int a = i + 2; a < partesFuncion.size(); a++)
								{
									partesFuncionMove.add(partesFuncion.get(a));
								}
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							else
							{
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							sumContador++;
							break;
						} else if ((i == 1 && partesFuncion.size() - 1 >= 3) && Character.isDigit(partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
							&& Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
							&& (partesFuncion.get(i + 2).toCharArray()[(partesFuncion.get(i + 2).length() - 1)] == '-' || partesFuncion.get(i + 2).toCharArray()[(partesFuncion.get(i + 2).length() - 1)] == '+'
							|| partesFuncion.get(i + 2).toCharArray()[(partesFuncion.get(i + 2).length() - 1)] == ')')
							)
						{
							//Aqui es cuan hay mas de tres elementos y esta al inicio
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
							partesFuncionMove.add(String.valueOf(Double.valueOf(partesFuncion.get(i - 1)) + Double.valueOf(partesFuncion.get(i + 1))));
							if ((i + 2) < partesFuncion.size())
							{
								for (int a = i + 2; a < partesFuncion.size(); a++)
								{
									partesFuncionMove.add(partesFuncion.get(a));
								}
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							else
							{
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							sumContador++;
							break;
						} else if ((i == partesFuncion.size() - 2 && partesFuncion.size() - 1 > 2)
							&& Character.isDigit(partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
							&& Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
							&& partesFuncion.get(i - 2).toCharArray()[partesFuncion.get(i - 2).length() - 1] == '-')
						{
							//Aqui es cuando hay mas de tres elementos y esta al final con una resta
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
							partesFuncionMove.add("+");
							partesFuncionMove.add(String.valueOf(Double.valueOf(partesFuncion.get(i - 1)) * (-1) + Double.valueOf(partesFuncion.get(i + 1))));
							if ((i + 2) < partesFuncion.size())
							{
								for (int a = i + 2; a < partesFuncion.size(); a++)
								{
									partesFuncionMove.add(partesFuncion.get(a));
								}
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							else
							{
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							sumContador++;
							break;
						}
						else if ((i == partesFuncion.size() - 2 && partesFuncion.size() - 1 > 2)
							&& Character.isDigit(partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
							&& Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
							&& (partesFuncion.get(i - 2).toCharArray()[partesFuncion.get(i - 2).length() - 1] == '+'
							|| partesFuncion.get(i - 2).toCharArray()[(partesFuncion.get(i - 2).length() - 1)] == '('))
						{
							//Aqui es cuando hay mas de tres elementos y esta al final con una suma
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
							partesFuncionMove.add(String.valueOf(Double.valueOf(partesFuncion.get(i - 1)) + Double.valueOf(partesFuncion.get(i + 1))));
							if ((i + 2) < partesFuncion.size())
							{
								for (int a = i + 2; a < partesFuncion.size(); a++)
								{
									partesFuncionMove.add(partesFuncion.get(a));
								}
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							else
							{
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							sumContador++;
							break;
						}
						else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
					}
					else if ((partesFuncion.get(i).toCharArray()[partesFuncion.get(i).length() - 1] == '+'))
					{
						if (Character.isDigit(partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
							&& Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
							&& (partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == '-' || partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == '+'
							|| partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == ')')
							&& (partesFuncion.get(i - 2).toCharArray()[(partesFuncion.get(i - 2).length() - 1)] == '(' || partesFuncion.get(i - 2).toCharArray()[partesFuncion.get(i - 2).length() - 1] == '+'))
						{
							//Aqui estas en el centro de la ecuacion y tienes una suma al inicio
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
							partesFuncionMove.add(String.valueOf(Double.valueOf(partesFuncion.get(i - 1)) + Double.valueOf(partesFuncion.get(i + 1))));
							if ((i + 2) < partesFuncion.size())
							{
								for (int a = i + 2; a < partesFuncion.size(); a++)
								{
									partesFuncionMove.add(partesFuncion.get(a));
								}
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							else
							{
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							sumContador++;
							break;
						}
						else if (Character.isDigit(partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
							&& Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
							&& (partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == '-' || partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == '+'
							|| partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == ')')
							&& partesFuncion.get(i - 2).toCharArray()[partesFuncion.get(i - 2).length() - 1] == '-')
						{
							//Aqui estas en el centro de la ecuacion y tienes una resta al inicio
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
							partesFuncionMove.add("+");
							partesFuncionMove.add(String.valueOf(Double.valueOf(partesFuncion.get(i - 1)) * (-1) + Double.valueOf(partesFuncion.get(i + 1))));
							if ((i + 2) < partesFuncion .size())
							{
								for (int a = i + 2; a < partesFuncion.size(); a++)
								{
									partesFuncionMove.add(partesFuncion.get(a));
								}
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.size();
							}
							else
							{
								partesFuncion.clear();
								for (int b = 0; b < partesFuncionMove.size(); b++)
								{
									partesFuncion.add(partesFuncionMove.get(b));
								}
								partesFuncionMove.clear();
							}
							sumContador++;
							break;
						}
						else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
					}
					else
					{
						partesFuncionMove.add(partesFuncion.get(i));
					}
				}
				sumContador--;
			}


        while (restContador > 0) {
            for (int i = 0; i < partesFuncion.size(); i++) {
                if ((i <= 1 || i >= partesFuncion.size() - 2) && (partesFuncion.get(i).toCharArray()[partesFuncion.get(i).length() - 1] == '-')) {
                    if ((i == 1 && partesFuncion.size() - 1 <= 2) && Character.isDigit
                        (partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
                                && Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]
                    
                        )))
						{
							
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.add(String.valueOf(Double.valueOf
                        (partesFuncion.get(i - 1)) - Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        sumContador++;
                        break;
                    }
                    else if ((i == 1 && partesFuncion.size() - 1 >= 3) && Character.isDigit
                                (partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
                                && Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
						  && ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
						  && ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
						  && (partesFuncion.get(i + 2).toCharArray()[(partesFuncion.get(i + 2).length() - 1)] == '-' || partesFuncion.get(i + 2).toCharArray()[(partesFuncion.get(i + 2).length() - 1)] == '+'
                            || partesFuncion.get(i + 2).toCharArray()[(partesFuncion.get(i + 2).length() - 1)] == ')'
                    
                        )
						  )
						{
							
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.add(String.valueOf(Double.valueOf
                        (partesFuncion.get(i - 1)) - Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        sumContador++;
                        break;
                    }
                    else if ((i == partesFuncion.size() - 2 && partesFuncion.size() - 1 > 2)
                            && Character.isDigit
                                (partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
                                && Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
						  && ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
						  && ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
						  && partesFuncion.get(i - 2).toCharArray()[partesFuncion.get(i - 2).length() - 1] == '-'
                    
                        )
						{
							
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.add("+");
                        partesFuncionMove.add(String.valueOf(Double.valueOf
                        (partesFuncion.get(i - 1)) * (-1) - Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        sumContador++;
                        break;
                    }
                    else if ((i == partesFuncion.size() - 2 && partesFuncion.size() - 1 > 2)
                            && Character.isDigit
                                (partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
                                && Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
							&& (partesFuncion.get(i - 2).toCharArray()[partesFuncion.get(i - 2).length() - 1] == '+'
                            || partesFuncion.get(i - 2).toCharArray()[(partesFuncion.get(i - 2).length() - 1)] == '('
                    
                        ))
						{
							
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.add(String.valueOf(Double.valueOf
                        (partesFuncion.get(i - 1)) - Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        sumContador++;
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else if ((partesFuncion.get(i).toCharArray()[partesFuncion.get(i).length() - 1] == '-')) {
                    if (Character.isDigit
                        (partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
                                && Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
							&& (partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == '-' || partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == '+'
                            || partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == ')')
							&& (partesFuncion.get(i - 2).toCharArray()[(partesFuncion.get(i - 2).length() - 1)] == '(' || partesFuncion.get(i - 2).toCharArray()[partesFuncion.get(i - 2).length() - 1] == '+'
                    
                        ))
						{
							
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.add(String.valueOf(Double.valueOf
                        (partesFuncion.get(i - 1)) - Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        sumContador++;
                        break;
                    }
                    else if (Character.isDigit
                                (partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1])
                                && Character.isDigit(partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
							&& ((partesFuncion.get(i - 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i - 1).toCharArray()[0]))
							&& ((partesFuncion.get(i + 1).toCharArray()[0]) == '-' || Character.isDigit(partesFuncion.get(i + 1).toCharArray()[0]))
							&& (partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == '-' || partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == '+'
                            || partesFuncion.get(i + 2).toCharArray()[partesFuncion.get(i + 2).length() - 1] == ')')
							&& partesFuncion.get(i - 2).toCharArray()[partesFuncion.get(i - 2).length() - 1] == '-'
                    
                        )
						{
							
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.add("+");
                        partesFuncionMove.add(String.valueOf(Double.valueOf
                        (partesFuncion.get(i - 1)) * (-1) - Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        sumContador++;
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            restContador--;
        }
        partesFuncionMove.clear();

        partesFuncionMove.clear();
        while (logContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                if (partesFuncion.get(i).toCharArray()[partesFuncion.get(i).toCharArray().length - 1] == 'l') {

                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
                    
                        )
						{
							partesFuncionMove.add(String.valueOf(Math.log10((Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}

                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            logContador--;
        }
        while (cosContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                if (partesFuncion.get(i).toCharArray()[partesFuncion.get(i).toCharArray().length - 1] == 'c') {

                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
                    
                        )
						{
							partesFuncionMove.add(String.valueOf(Math.cos((Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}

                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            cosContador--;
        }
        while (powContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                if (partesFuncion.get(i).toCharArray()[partesFuncion.get(i).toCharArray().length - 1] == '^') {

                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1]) && Character.isDigit(partesFuncion.get(i - 1).toCharArray()[partesFuncion.get(i - 1).length() - 1]
                    
                        ))
						{
							partesFuncionMove.remove(partesFuncionMove.size() - 1);
                        partesFuncionMove.add(String.valueOf(Math.pow(Double.valueOf
                        (partesFuncion.get(i - 1)), Double.valueOf
                        (partesFuncion.get(i + 1)
                        )))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}

                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            powContador--;
        }

        while (sqrtContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                //MessageBox.Show("Si entras al metodo "+i);
                if (partesFuncion.get(i).toCharArray()[0] == 'r') {

                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
                    
                        )
						{
						
							partesFuncionMove.add(String.valueOf(Math.sqrt((Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        sqrtContador++;
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            sqrtContador--;
        }

        while (absContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                //MessageBox.Show("Si entras al metodo "+i);
                if (partesFuncion.get(i).toCharArray()[0] == 'a') {

                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
                    
                        )
						{
							partesFuncionMove.add(String.valueOf(Math.abs((Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        absContador++;
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            absContador--;
        }
        while (senoContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                if (partesFuncion.get(i).toCharArray()[0] == 's') {
                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
                    
                        )
						{
							partesFuncionMove.add(String.valueOf(Math.sin((Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        senoContador++;
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            senoContador--;
        }

        while (tanContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                if (partesFuncion.get(i).toCharArray()[0] == 't') {
                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
                    
                        )
						{
							partesFuncionMove.add(String.valueOf(Math.tan((Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        tanContador++;
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            tanContador--;
        }

        while (arcCosContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                if (partesFuncion.get(i).toCharArray()[0] == 'v') {
                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
                    
                        )
						{
							partesFuncionMove.add(String.valueOf(Math.acos((Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        arcCosContador++;
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            arcCosContador--;
        }
        while (arcSenContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                if (partesFuncion.get(i).toCharArray()[0] == 'd') {
                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
                    
                        )
						{
							partesFuncionMove.add(String.valueOf(Math.asin((Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        arcSenContador++;
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            arcSenContador--;
        }
        while (arcTanContador > 0) {
            for (int i = 0; i < partesFuncion.size() - 1; i++) {
                if (partesFuncion.get(i).toCharArray()[0] == 'y') {
                    if (Character.isDigit
                        (partesFuncion.get(i + 1).toCharArray()[partesFuncion.get(i + 1).length() - 1])
                    
                        )
						{
							partesFuncionMove.add(String.valueOf(Math.atan((Double.valueOf
                        (partesFuncion.get(i + 1)
                        ))))
                        );
							if ((i + 2) < partesFuncion.size()) {
                            for (int a = i + 2; a < partesFuncion.size(); a++) {
                                partesFuncionMove.add(partesFuncion.get(a));
                            }
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        } else {
                            partesFuncion.clear();
                            for (int b = 0; b < partesFuncionMove.size(); b++) {
                                partesFuncion.add(partesFuncionMove.get(b));
                            }
                            partesFuncionMove.clear();
                        }
                        arcTanContador++;
                        break;
                    }
                    else
						{
							partesFuncionMove.add(partesFuncion.get(i));
						}
                } else {
                    partesFuncionMove.add(partesFuncion.get(i));
                }
            }
            arcTanContador--;
        }

        return partesFuncion;
    }
}