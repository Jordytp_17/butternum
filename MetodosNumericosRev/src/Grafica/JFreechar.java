/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Grafica;

import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author nesto
 */
public class JFreechar {
   JFreeChart grafica; 
   XYSeriesCollection datos= new XYSeriesCollection();
   String titulo;
   String etiquetax;
   String etiquetay;
   public JFreechar(String t,String x,String y){
       etiquetax=x;
       etiquetay=y;
       titulo=t;
       grafica= ChartFactory.createXYLineChart(titulo, x, y, datos, PlotOrientation.VERTICAL, true, true, true);
   }
   public JFreechar(){
       this("Grafica","x","y");
   }
   public void AgregarGrafica(String id,float [] x,float [] y){
       XYSeries graf= new XYSeries(id);
       int n=x.length;
       for(int i=0;i<n;i++){
           graf.add(x[i],y[i]);
       }
       datos.addSeries(graf);
   }
   public void CrearGrafica(String id,float [] x,float [] y){
       datos.removeAllSeries();
       AgregarGrafica(id, x, y);
   }
   public JPanel obtenerGrafica(){
      return new ChartPanel(grafica); 
   }  
}

